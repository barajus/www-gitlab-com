---
layout: handbook-page-toc
title: Webcasts
description: An overview of webcasts at GitLab, including processes for Zoom and BrightTALk.
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

---

## GitLab-Hosted Webcasts Calendar
{:#calendar .gitlab-purple} <!-- DO NOT CHANGE THIS ANCHOR -->

<figure>
<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23B39DDB&amp;ctz=America%2FLos_Angeles&amp;src=Z2l0bGFiLmNvbV9uMnNibXZmMjlqczBzM3BiM2ozaHRwa3FmZ0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Z2l0bGFiLmNvbV8xOGVqOHN0NmlxajZpYXB1NTNrajUzNHBsa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%23039BE5&amp;color=%239E69AF&amp;color=%23009688&amp;title=All%20GitLab%20Virtual%20Events&amp;showCalendars=1" style="border:solid 1px #777" width="1000" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

[See calendar glossary here](/handbook/marketing/virtual-events/#calendar-glossary)

## BrightTALK Webcasts
{:#brighttalk .gitlab-purple} <!-- DO NOT CHANGE THIS ANCHOR -->

### Quick Links
{:#bt-quick-links} <!-- DO NOT CHANGE THIS ANCHOR -->

* [BrightTALK Support Portal](https://support.brighttalk.com/hc/en-us)
* [BrightTALK Academy](https://business.brighttalk.com/academy/)

### Types of BrightTALK Webcasts
{:#bt-types} <!-- DO NOT CHANGE THIS ANCHOR -->

* **TALK (Live):** traditional live webcast with live speakers and Q&A.
   - Pre-event registration takes place
   - The event automatically converts to on-demand upon completion
* **RP (Replay)**: recorded webcast that appears to be hosted live
   - Pre-event registration takes place
   - At time of webcast, a recording is played. There is no live Q&A (technically), but it is best to do a mock Q&A and inform attendees that any questions will be individually addressed after the webinar, allowing for seamless follow up by SDRs.
   - The event automatically converts to on-demand upon completion
* **VD (Video)**: essentially an on-demand webcast acting as content syndication into the BrightTALK channel
   - No pre-registration

### BrightTALK Webcast Calendar
{:#bt-calendar} <!-- DO NOT CHANGE THIS ANCHOR -->

The Campaigns Team manages the [BrightTALK webcast calendar](https://calendar.google.com/calendar/u/1?cid=Y19xdTVqMzRsZ2ZrcmlybmM5aGx1MWRkams0MEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t), which will be used to log all planned and scheduled BrightTALK webcasts and their related dry runs. **The purpose of the BrightTALK webcast calendar is to avoid scheduling overlapping webcasts and to provide visibility into all BrightTALK webcasts.**

**Adding to the calendar:**
1. Add tentative webcasts to the calendar with `[Hold]`
   * ex. `[Hold] TALK | 7 secrets of effective GitOps`
   * For dry run (practice) webcasts, add `DR` (ex. `[Hold DR] TALK | 7 secrets of effective gitops`)
   * Include a link to the issue in the event description
1. When the date/time/speakers of the webcast has been confirmed, remove `[Hold]` from the event title
   * ex. `TALK | 7 secrets of effective GitOps` or `RP | 7 secrets of effective GitOps`
   * Add the time of the webcast on the calendar event (if it is still an all-day event)
   * Add the epic link, and remove the issue link, in the calendar description
   * Add all presenters (internal GitLab team members and external speakers)
   * Add BrightTALK login information for presenters

## Project Management for BrightTALK Webcasts
{:#bt-project-management .gitlab-purple} <!-- DO NOT CHANGE THIS ANCHOR -->
Campaign webcasts are hosted on BrightTALK, and project management is organized by Campaign Managers.

### Organizing BrightTALK webcast epics and issues
{:#bt-epics-issues} <!-- DO NOT CHANGE THIS ANCHOR -->

* **Confirm Date:** The webcast idea issue (date request issue) must be complete and confirmed before creation of the epic, issues, and workback.
* **Campaign Webcast Epic:** campaign manager creates webcast epic (using code below)
* **Related Issues:** campaign manager creates the issues as designated in the GANTT sheet, and associates to the campaign webcast epic

[View the webcast workback timeline GANTT here](https://docs.google.com/spreadsheets/d/1A4c2OodEAsOlN4Ek-rBiLlwkdF0AvX5YBiY4mhkZd-M/edit#gid=666473040)

```
<!-- Naming convention: [Webcast Title] - [3-letter Month] [Date], [Year] -->

## [GANTT >>]() - [owner to copy from this template](https://docs.google.com/spreadsheets/d/1A4c2OodEAsOlN4Ek-rBiLlwkdF0AvX5YBiY4mhkZd-M/edit#gid=666473040)

## [Landing Page >>]() - `to be added when live`

#### :key: Key Details
* **Webcast DRI:** 
* **Speaker(s) and Moderator:** 
* **Official Webcast Name:** 
* **Official Webcast Date:** 
* **Sales Segment:** `Large, Mid-Market, or SMB`
* **Lifecycle Stage/Lead Status:** `Raw, Inquiry, MQL, Accepted, Qualifying, Qualified, Nurture`
* **Sales Region:** `AMER, EMEA, APAC`
* **Sales Territory (if specific):** 
* **Goal:** `Please be specific on the KPI this is meant to impact. For example, drive MQLs against named account list, increase velocity of MQLs > SAOs, increase velocity of early stage opps to close.`
* [landing page copy]() - `doc to be added by Marketing Programs` ([clone the template here](https://docs.google.com/document/d/1xHnLKPCaXrpEe1ccRh_7-IqgNbAlzQsZVc-wr1W4ng8/edit#))
* [ ] [main salesforce campaign]()
* [ ] [main marketo program]()

## :books: Issue creation

<details>
<summary>Expand below for checkboxes of issues to be created, use the GANTT to calculate the due dates.</summary>

* [ ] Zoom license date request issue created
* [ ] Secure presenters and schedule dry runs issue created
* [ ] Facilitate tracking issue
* [ ] Landing page issue created
* [ ] Optional: New design assets issue created for the design team
* [ ] Invitation and reminder issue created
* [ ] Organic social issue created for social media manager
* [ ] Paid Ads issue created for DMP
* [ ] PathFactory request issue created
* [ ] Follow up email issue created
* [ ] Add to nurture stream issue created
* [ ] Host dry run issue created
* [ ] Prepare for webcast isue created
* [ ] On-demand switch issue created
</details>

/label ~mktg-demandgen ~dg-campaigns ~"Webcast - GitLab Hosted" ~"Webcast" ~"mktg-status::wip"

```

## BrightTALK LIVE webcast registration and tracking
{:#bt-setup .gitlab-purple} <!-- DO NOT CHANGE THIS ANCHOR -->

### Create program in Marketo
{:#bt-marketo-program} <!-- DO NOT CHANGE THIS ANCHOR -->

1. Create the webcast program in Marketo by navigating to the [Webcast program template for BrightTalk](https://engage-ab.marketo.com/?munchkinId=194-VVC-221#/classic/ME6946A1)
1. Right-click and select "clone"
1. Next to "Clone To", choose `A campaign folder`.
1. For "Name", add the MKTO program name (this is also the SFDC campaign name). Use the following format: `YYYYMMDD_{Webcast Title}_[Region - only if applicable]`. For example, `20170418_MovingToGit`.
1. Under "Folder", choose the appropriate quarter within the  `GitLab-Hosted Campaign Webcasts` folder.
1. Click "Create" (note - you will create the SFDC campaign from Marketo in the next step!)

### Create campaign in Salesforce
{:#bt-sfdc-campaign} <!-- DO NOT CHANGE THIS ANCHOR -->

1. In the Marketo program Summary view, you will see `Salesforce Campaign Sync:` with a link that says "not set".
1. Click on "not set" 
1. Where it says "None", click the drop-down and choose "Create New"
1. The Marketo program name will auto-fill for the name (for consistency across both systems)
1. In the "Description", add a link to the epic
1. Click "Save"
1. NOW you will navigate to the Campaign in SFDC to do a quick review - [Shortcut to Campaigns](https://gitlab.my.salesforce.com/701/o)
1. Click into the SFDC campaign
1. Change the campaign owner to the webcast DRI
1. Change the status to `in progress`
1. Edit the `Enable Bizible Touchpoints` field to `Include only "Responded" Campaign Members`
1. Edit the Budgeted Cost (required) to cost of webcast, or "$1" if there is no cost associated
1. Click save

### Create webcast in BrightTALK
{:#bt-create-webcast} <!-- DO NOT CHANGE THIS ANCHOR -->

1. **LOGIN**: log into BrightTALK, go into the Content tab, and select `Add content`, schedule a talk. _*Note: Only type: `talk` allows you to share videos_
1. **TALK TITLE**: Insert the title of the webcast (public facing)
1. **DESCRIPTION**: Enter here a brief synopsis of the webcast. Keeping this description relevant to the content of the webcast will make it easier for viewers to search for it.
1. **PRESENTER**: Enter the name(s) of the presenter(s) who will be delivering the webcasts. Role and company can be included also.
1. **DURATION**: Add how long the webcast will be.
1. **START DATE**: Select the date your live webcast will take place on.
1. **START TIME**: Time your webcast will go live.
1. **TIMEZONE**: The time zone you select here should be based on where your presenter intends to present from. This will determine the local dial-in number generated for your presenter. NOTE: This will NOT affect how your webcast is listed. Webcasts are always listed in your player and on BrightTALK in the local time of your viewers.
1. **TAG**: Enter up to 10 terms that cover the topics and themes of your content - simply type each tag and click 'Add tag'. BrightTalk will suggest topics that are trending, but feel free to add any tag you believe is relevant - up to 34 characters per tag.
1. **IMAGE UPLOAD**: This will be used for the click to play overlay and thumbnail. Upload JPG/PNG image file of size 640x360. File upload limit 1MB.

PUBLISHING
1. **Public/Private**: Select 'Public' to promote this webcast in your channel listing and via the BrightTALK email service. Select 'Practice (Private)' to run this webcast without it being promoted in your channel listing or via the BrightTALK email service. Only viewers with a direct link to the 'Practice (Private)' webcast will be able to view it.
1. **Channel Survey**: Select 'Enabled' to allow surveys to go out for this webcast. Otherwise, select 'Disabled'.
1. **Campaign Reference**: Insert name of the Marketo Campaign **exactly** as shown in Marketo. Once added here, any changes to the Marketo program name will *BREAK* the sync! If you have a date change for your webcast, just leave it as-is in Marketo if the sync has already been set up.
1. **Add to BrightTalk Communities**: Control which BrightTALK communities to promote this webcast into by adding them below. The 'Primary community' you select will be the focus for promotional activities such as the BrightTALK email service.

### Connect the Marketo program to BrightTALK
{:#bt-marketo-connect} <!-- DO NOT CHANGE THIS ANCHOR -->

1. Navigate to the [Connectors Tab](https://www.brighttalk.com/central/account/20277/channel/17523/connectors) in BrightTALK Demand Central
1. Click to [Manage](https://www.brighttalk.com/central/account/20277/channel/17523/connector/1579/manage) under the Marketo Account
1. Navigate to `Marketo Programs`, find your program and select `Connect`
1. Insert program name EXACTLY as it is spelled in marketo, and click Next. *CRITIACL NOTE: If you change the program name, the sync will break.
1. Select how far back you want to sync data for, typically put in today's date. Click Next.
1. You are now connected! The sync runs every hour.

### Update tokens in Marketo program
{:#bt-marketo-tokens} <!-- DO NOT CHANGE THIS ANCHOR -->

1. There is no need to update ALL Tokens at this time, as all registration and emails are being sent from the BrightTALK platform. Update the following Tokens:
   * `{{my.bullet1}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet2}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet3}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.bullet4}}` - bullet copy with approved [character limits](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=43971442)
   * `{{my.mpm owner email address}}` - not used in automation, but helpful to know who to go to about setup
   * `{{my.socialImage}}` - image that would be presented in social, slack, etc. preview when the URL is shared, this image is provided by design/social, leave the default unless presented with webcast specific image.
   * REPEAT this for speaker 2 and 3. If there are more or less speakers, follow the instructions below at the end of the general webcast setup.
   * `{{my.utm}}` - UTM to track traffic to the proper campaign in reporting dashboards (append integrated campaign utm or program name, if webcast is not part of an integrated campaign, to the utm campaign token)
   * `{{my.valueStatement}}` token with the short value statement on what the viewer gains from the webcast, this ties into the follow up emails and must meet the max/min requirements of the [character limit checker](https://docs.google.com/spreadsheets/d/1dKVIZGbbOLoR5BdCqXqCQ40qJlQNif9waTiHc8yWggQ/edit#gid=1471341556)
   * `{{my.webcastDate}}` - the webcast LIVE date.
   * `{{my.webcastDescription}}` - 2-3 sentences with approved character limits, this will show up in page previews on social and be used in Youtube and Pathfactory description.
   * `{{my.webcastSubtitle}}` token with subtitle for the webcast.
   * `{{my.webcastTime}}` token with the webcast time in local timezone/UTC timezone.
   * `{{my.webcastTitle}}` token with the webcast title.

### Activate smart campaigns in Marketo
{:#bt-marketo-smart-campaigns} <!-- DO NOT CHANGE THIS ANCHOR -->

   * Activate the `01 Processing` campaign.
   * Schedule the `02 Set No-Show Stats` smart campaign for 3-4 hours AFTER the webinar will end.
   * Interesting moments are captured on a global level.

### Schedule your BrightTALK practice session (Dry Run)
{:#bt-practice-session} <!-- DO NOT CHANGE THIS ANCHOR -->

* Create a Talk, following the [instructions above.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/)
* Select 'Practice (Private)' to run this webcast without it being promoted in your channel listing or via the BrightTALK email service. Only viewers with a direct link to the 'Practice (Private)' webcast will be able to view it.
* After the practice session, the system will automatically `Publish` your dry run. When your dry run completes, go into the Content section of your channel, Manage the dry run, click Edit, and change it to `Unpublish`.
* If you watch the replay of the practice session and notice that slides, video, or demo look grainy, select the gear button on the video and change the resolution of the webcast.

### Loading a video to replay in BrightTALK
{:#bt-video-replay} <!-- DO NOT CHANGE THIS ANCHOR -->

* You must upload the video you plan to play in a live talk prior to the event.
1. **LOGIN**: log into BrightTALK, go into the Content tab, and select `Add content`, Upload a Video. Videos are uploaded as Unpublished.
1. **WEBCAST TITLE**: Insert the title of the video (public facing)
1. **DESCRIPTION**: Enter here a brief synopsis of the video. Keeping this description relevant to the content of the webcast will make it easier for viewers to search for it if you plan to make this public.
1. **PRESENTER**: Enter the name(s) of the presenter(s) who will be delivering the webcasts. Role and company can be included also.
1. **DATE**: Select the date of upload (or date of the video)
1. **TAG**: Enter up to 10 terms that cover the topics and themes of your content - simply type each tag and click 'Add tag'. BrightTalk will suggest topics that are trending, but feel free to add any tag you believe is relevant - up to 34 characters per tag.
1. **DON'T PROMOTE**: If you only want to play this video in a live webcast, select `Don't promote this webcast into any communities`
1. Click Proceed
1. Select the video for upload. Add the time stamp to capture an image, or upload a featured image.
1. Select the nearest upload location to you.
1. Click upload. This may take awhile, depending on how large the video is.
1. After the video uploads, click `Edit and publish`. Change the video to `Private` if you are going to play it during a live webcast.
1. You can now select the video to play from the presenter screen of your Talk.

### BrightTALK Webcast Invitations
{:#bt-email-invitations} <!-- DO NOT CHANGE THIS ANCHOR -->

:exclamation: **Note from @jgragnola: we are working on further templatizing these invitations so that copy changes are not needed and tokens take care of these emails.** ([issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/3422))

1. Update emails `Invitation 1 - 2 weeks prior`,  `Invitation 2 - 1 week prior` , and if needed `Invitation 3 - Day before` with relevant copies related to the webcast.
   * *Note: We normally use the same copy for all 3 emails and simply tweak the templated subject lines to sound more like “Reminders”.*
2. Approve copy and send samples to the requester, and the presenter (if different from requester).
3. Go to the `List` folder and edit the `Target List` smart list and input the names of past similar programs and applicable program statuses to the `Member of program` filter. This will make sure people that have attended programs with similar topics in the past are included in the invite.
   * Note: We may uplevel this logic to include webcast invitations within active nurture programs to limit the overhead for invitation emails.
4. Once you get approval on the sample email copy, schedule the email programs outlined in step 1.

### Add the webcast to the /events/ and /resources/ pages
{:#bt-webpage-listings} <!-- DO NOT CHANGE THIS ANCHOR -->

*  To add the webcast to the /events page follow this [step by step guide](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents).
*  To add the webcast to the /events page follow this [step by step guide](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#add-new-content-to-the-resources-page).

### BrightTALK Webcast Testings / QA
{:#bt-webcast-testing} <!-- DO NOT CHANGE THIS ANCHOR -->

Submit a test lead using your gitlab email on the LIVE landing page to make sure the registration is tracked appropriately in the Marketo program and you get a confirmation email from BrightTALK.

:stopwatch: It will take up to 2 hours to sync the regisration from BrightTalk to Marketo.

### Switching BrightTALK Webcasts to On-Demand
{:#bt-ondemand-switch} <!-- DO NOT CHANGE THIS ANCHOR -->

BrightTALk will automatically convert the video to on-demand in the BrightTALK platform. The steps below allow us to further leverage the webcast in Pathfactory.

1. **Youtube**: Upload the recording to our main GitLab channel
   * Fill in the title with the webcast title matching the Marketo token (`{{my.webcastTitle}}`)
   * Fill in the description with the short description matching the Marketo tokens (`{{my.contentDescription}}`)
   * Make sure the video is set as `Unlisted` so only people with the URL to the video can find it
1. **Youtube**: Once the recording has been uploaded, copy the video link on the right
1. **Pathfactory**: Login to PathFactory and add the copied youtube link to Pathfactory as new content by following the instructions outlined [here](/handbook/marketing/marketing-operations/pathfactory).

### Rescheduling a BrightTALK webcast
{:#bt-reschedule} <!-- DO NOT CHANGE THIS ANCHOR -->

In the event you need to change the date of your webcast, please follow the steps outlined below.

1. DO NOT UPDATE THE PROGRAM NAME IN MARKETO - this will break the sync if it is already set up between Marketo and BrightTALK.
1. Update the date/time of the webcast on the webcast calendar and resend invites to all panelists.
1. Update the webcast epic so the new date is reflected on the title
1. Leave a comment on the epic stating the event has been rescheduled and tag all internal panelists and hosts.
1. Update issue due dates based on the new timeline and communicate changes to relevant team members.
1. Update on the [events page](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/events.yml) and [resources page](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/resources.yml).
1. Update the date in BrightTALK
   * *An email will automatically send from BrightTALK to the registrants.*

### Canceling a BrightTALK Webcast
{:#bt-cancel} <!-- DO NOT CHANGE THIS ANCHOR -->

In the event you need to cancel your webcast, please follow the steps outlined below. Note that BrightTalk sets up promotional emails 24-48 hours in advance. This means that after you cancel a webcast, it may still appear in promotional emails (via webcast recommendations) from BrightTalk for up to 48 hours. However, they will not be able to register for the cancelled webcast.

1. Remove the webcast from the BrightTALK calendar.
1. Add [Cancelled] to the webcast epic title then close it out.
1. Leave a comment on the epic stating the event has been canceled and tag all internal panelists and hosts.
1. Add [Cancelled] to the related issues and close them out.
1. If webcast is on the Events Page and Resources Page, remove in a new MR.
1. Go into BrightTalk and Cancel
   * *An email will automatically send from BrightTALK to the registrants.*
1. In the Marketo program, deactivate all active smart campaigns and append [Cancelled] to the program name. NOTE: once you do this, the sync between Marketo and BrightTALK will be broken and there is no reverse.
1. Go to Salesforce, append [Cancelled] to the SFDC campaign name.








## Zoom Webcast Calendar

The Field Marketing team manages the [Zoom webcast license calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV8xcXZlNmc4MWRwOTFyOWhldnRrZmQ5cjA5OEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t), which will be used to track all planned and scheduled Zoom webcasts/workshops and their related dry runs. **The purpose of the Zoom webcast calendar is to avoid scheduling overlapping webcasts when using the single license and to provide executive visibility into all Zoom webcasts being hosted.**

