---
layout: handbook-page-toc
title: Product Handbook
description: "Most of the content in the product handbook talks about _how_ we do product at GitLab."
---

## Welcome to the Product Team Handbook

- Product handbook focuses on _how_ we do product at GitLab.
- Please see the [Product Direction](/direction/) for _what_ the team plans to build.
- Product is led by [Scott Williamson](/company/team/#sfwgitlab), [Chief Product Officer](/job-families/product/chief-product-officer/).
- If you'd like to collaborate with product management see the [How to Engage](/handbook/product/how-to-engage/) guide.
### Product Team Mission

**We create products and experiences that customers love and value.**

- **Consistency wins as you scale**. Our organizational goal is to create a Product Manager (PM) system that maximizes the chances of success across new products, new features, and new team members.
- **We are shipping an experience**, and not just a product. Don’t forget about the links between the product and the website, pricing & packaging, documentation, sales, support, etc.
- **It’s about our customers and doing a job for them**, not the product itself. Think externally about customer problems, not internally about the technology.
- **It’s about love AND value**. Will customers value what we are building? We need to make sure what we build helps build and extract customer value.

#### [**Product Principles**](/handbook/product/product-principles)

The Product Principles section is where you can learn about our strategy and philosophy regarding product development here at GitLab.

#### [**Product Processes**](/handbook/product/product-processes)

For a detailed view on how we do Product Development, read up on our established Product Processes.

#### [**Product Categorization**](/handbook/product/categories/)

To learn how the GitLab product and our Teams are organized review our Product Categorization section.

#### [**About the GitLab Product**](/handbook/product/gitlab-the-product)

Learn about GitLab as a product, including what does it mean to be a single application, our subscription tiers and pricing model, and the basics of permissions in the platform.

#### [**Being a Product Manager at GitLab**](/handbook/product/product-manager-role)

Want to know more on what being a Product Manager at GitLab is like? Checkout our [Product Manager Role](/handbook/product/product-manager-role/) guide for helpful information like our Career Development Framework and the Roles and Responsibilities of a PM.

#### [**Product Performance Indicators**](/handbook/product/performance-indicators/)

Learn how we measure success in Product via our Product KPIs, which are tracked in our [Product project](https://gitlab.com/gitlab-com/Product). For best practices and guidance on how to add instrumentation for features please review our [Product Intelligence workflow](/handbook/product/performance-indicators/#product-intelligence-workflow).

#### [**Product OKRs**](/handbook/product/product-okrs/)

Understand the OKR Process for the GitLab Product Team and review current and past OKRs.

#### [**Our Product Leadership Team**](/handbook/product/product-leadership)

Learn about our Product Leadership Team and learn about them via their personal README’s.

#### Popular Product Resources
- [**Data for Product Managers**](/handbook/business-ops/data-team/data-for-product-managers/)
- [**Learning and Development for Product Management**](/handbook/product/product-manager-role/learning-and-development/)
- [**Product Operations Releases**](/handbook/product/product-operations/)
- [**Product Manager responsibilities**](/handbook/product/product-manager-responsibilities/)
- [**Product Management CDF and competencies**](/handbook/product/product-manager-role/product-CDF-competencies/)
- [**Product Development Flow**](/handbook/product-development-flow/)
- [**Product Development Timeline**](/handbook/engineering/workflow/#product-development-timeline)
- [**Product Intelligence Guide**](/handbook/product/product-intelligence-guide)
- [**Product Pricing Model**](/company/pricing/)
- [**Tiering Guidance for Features**](https://about.gitlab.com/handbook/product/tiering-guidance-for-features/#learning-opportunities)
- [**Release Posts**](/handbook/marketing/blog/release-posts/)
- [**Product Budgeting Process**](/handbook/product-development-budgeting/)

### Communicating with the Product Team

GitLab team members should leverage [How to Engage](https://about.gitlab.com/handbook/product/how-to-engage/) to interact with the product team.

Members of the Product team are added to the [private product group](https://gitlab.com/gl-product) `@gl-product`. This group is used for internal communication and the `@gl-product` mention can only be used by project members.
Please remember that tagging `@gl-product` on issues will generate in-product [to-do items](https://docs.gitlab.com/ee/user/todos.html) and email notifications to **all** product team members, so use it only when you need to communicate with the entire product team.

If you are tagging `@gl-product`:

- Clearly state why you are tagging the entire product team and what action you need product team members to take.
- Write a short summary in the same comment so team members can quickly understand the necessary context.
- Review the issue title and description to ensure it has relevant details other product team members need **BEFORE** submitting the comment. The issue title will be the subject of email notifications and in-product to-do items.
- If asking team members to review a change, please directly link to the specific page on the [review app](https://docs.gitlab.com/ee/ci/review_apps/#how-review-apps-work) and any relevant issues or MRs.

##### Required Knowledge for Product Managers

<kbd class="required">Required 🔎</kbd>

The Product handbook contains a lot of content that includes a mix of advice, best-practices,
mandatory process, team-specific content, and more. To help you find the most important
items, look for the badge above that indicate the most essential, important, and/or
obligatory processes that every Product Manager is expected to know by heart and follow,
as opposed to other items which may be more for reference in certain situations or may
be more optional than required. If you are adding or updating an essential handbook section,
consider adding this badge there.

#### Adding to or updating the product handbook 

For all MRs, please apply `milestone` and labels `product operations` `product handbook` and `prodops:release` so your changes show up in the product handbook [updates page](https://about.gitlab.com/handbook/product/handbook-updates/). 

Please add  [Product Operations](/company/team/#fseifoddini) in the merge request as a Reviewer. For small improvements (such as fixing typos, content clarifications), no need to await feedback from Product Operations, please merge the MR yourself (if you have permissions) . For larger changes, please request review or seek collaboration with Product Operations. 

If you have a larger change such as wanting to add new sections, significantly modify content in an existing section, or adding entirely new pages or processes, please create a merge request and direct ping [Product Operations DRI](/company/team/#fseifoddini) to request review, seek collaboration and possibly approval.

If you're looking ot create an entirely new directory, it is recommended you discuss in advance with product operations to avoid needing to move it or resturcture it later. 

#### Informing team members about the changes

Please note it is your responsibility to inform team members as needed about your merge request. Consider the following best practices:

- If it's a significant change that broadly affects the whole product team and/or the Engineering team, please consult Product operations for the best communication strategy. Otherwise, please follow the suggestions:
     - If only specifically relevant to product management, tag '@gl-product' in the MR prior to merging
     - If relevant to other teams, tag department leads such as the VP of UX, VP of Development or the Director of Quality Engineering in the MR prior to merging
     - Share and cross post the MR link with a brief description in relevant channels such as Slack #product, #product-leadership, #eng-managers, #ux-managers
     - Add as a read-only or discussion topic as appropriate in the the [Weekly Product Management Meeting](/handbook/product/product-processes/#weekly-product-management-meeting)
     - If you need more guidance on how/where/when to share, check out more [tips and best practices](/handbook/communication/#top-tips-and-best-practices) in [GitLab Communication](/handbook/communication/)
