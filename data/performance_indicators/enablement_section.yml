- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: 1.3
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - Page load times have decreased in July, to nearly the target of 1.3.
  implementation:
    status: Complete
    reasons:
      - We are working to [directly tie page load data to routes](https://gitlab.com/gitlab-org/gitlab/-/issues/331807).
  lessons:
    learned:
    - Page load times are not significantly affected by geography, with median times of 1.1s for US, 1.2 for most of Europe, and 1.3 for Australia. It seems internet latency is not a significant driver of overall performance for .com, however pages with many async requests will see higher impact. 
    - We are still working to [directly tie page load data to routes](https://gitlab.com/gitlab-org/gitlab/-/issues/331807), the data pipeline was updated but content does not seem to be flowing into Snowflake. 
    - We are also working to include RUM data (frontend performance) in error budgets, but pulling it into Grafana has proven [more difficult](https://gitlab.com/gitlab-data/analytics/-/issues/8733#note_603067403) than originally thought. We will need to update our lambda to push to Prometheus. 
    - Also exploring a [CI test suite](https://gitlab.com/gitlab-org/gitlab/-/issues/331808) to catch regressions before they make it to production.
  urls:
      - https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12056437&udv=0
      - https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12057815&udv=0
  metric_name: performanceTiming
  sisense_data:
    chart: 10546836
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Bottom line - % on latest 3 versions continues to be stable, eventhough the median age of version continues to generally increase. There was a noticable drop in median age of instance for CE users.
    - There is a need to continue to narrow down the factors that have the largest impact on a decision to upgrade by the instance owner. There are a few hypotheses for what affects the choice to upgrade, but it is difficult to tie those impacts directly to data points.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - The [most recent update](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380) to the median age of license most notably shows a large dip in median age for CE users. A previous research issue about [how can we prompt community edition users to upgrade more regularly](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/844) was opened, but research was not conducted yet on the subject. This warrants a higher priority based on this new data point.
    - Security was a central focus for admins to upgrade their version from the [ease of upgrading study](https://gitlab.com/gitlab-org/ux-research/-/issues/1114). The initial MVC has been merged, to [prompt users to sign up for the security newsletter](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/62502) in the next steps docs after installation. The next iteration will be to [prompt current users to sign up for the security newsletter in the admin console](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/842), which is prioritized for completion in the next few milestones.
    - Another important aspect for admins to upgrade was [when there would be the least impact to developers](https://gitlab.com/gitlab-org/ux-research/-/issues/1114). The MVC would be to [display this activity to admins](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/845), and showcase the time of least activity, with a call to action to "upgrade at this time". It has been determined the data is available and there will be a discussion if this is something worth implementing. This would lay important groundwork for "auto-upgrades", similar to the experience an individual might have with their personal laptop operating system updates.
    - Work has begun on [better highlighting](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators. This will impact results once it has been completed. A dependent issue to [add JSON formatting](https://gitlab.com/gitlab-services/version-gitlab-com/-/issues/421) was found in 14.2 blocking this upgrade, which has delayed completion to 14.3 or 14.4.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) to the usage data. The data is [now available in Sisense](https://app.periscopedata.com/app/gitlab/885304/Geo-Node-Status-Usage-Data?widget=12066071&udv=0), and more work is needed to produce a usable chart. 
    - We also [added git push metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/320984) to the usage data. However we are [not seeing reported data](https://app.periscopedata.com/app/gitlab/885304/Geo-Node-Status-Usage-Data?widget=12066132&udv=0) and must investigate further.
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we may now consider incorporating git fetch metrics now that they are instrumented. We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. We've recently completed the feature work to make the secondary UI indistinguishable from the primary. Solution validation is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - The number of users is still very low, likely because the WebUI is insufficient. We are working to change this. See [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing) and in-progress implementation work in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2

- name: Enablement:Geo - Paid GMAU - Number of potential Geo users
  base_path: "/handbook/product/performance-indicators/"
  definition: Sum of all users who are using instances with Geo enabled. All these users are positively impacted due to Geo in case of a failover or restore from backup. 
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster, but if one occurs everyone benefits. Based on [the node number distribution](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=6471733&udv=0) ~60% of our customers use Geo mainly for DR. I think this is worth measuring because setting up Geo is always a conscious decision by the customer - it must be configured.
    - We are working to add support for replicating and verifying all data types, so that Geo is on a solid foundation for both DR and Geo Replication.
    - In 14.2 we replicate ~86% of all data types (25 out of 29 in total) and verify ~48%. Replication for GitLab Pages and verification for LFS Objects and External MR Diffs planned for 14.3.
  implementation:
    status: Instrumentation
    reasons:
    - Geo customers and user data available but we can't distinguish DR use case from replication yet.
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 9939914
    dashboard: 500159
    embed: v2

- name: Enablement:Geo - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-geo/stage-groups-group-dashboard-enablement-geo)
    - Geo is not deployed on .com - This currently monitors one sidekiq worker that checks whether Geo is enabled.\
  sisense_data:
    chart: 12197844
    dashboard: 892802
    embed: v2
  sisense_data_secondary:
    chart: 12197848
    dashboard: 892802
    embed: v2

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
    - Plot displays a 13.13 release for unknown reasons. Investigating
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption remains stable; 14.2 data to early to analyze.
    - To further reduce memory consumption, we are planning to work on [splitting the application into functional parts to ensure that only needed code is loaded](https://gitlab.com/groups/gitlab-org/-/epics/5278) and on [reducing the memory consumption for Puma and Sidekiq endpoints](https://gitlab.com/groups/gitlab-org/-/epics/5622).
    - Working towards [upgrading Ruby from 2.7 to Ruby 3](https://gitlab.com/groups/gitlab-org/-/epics/5149). This may result in overall performance improvements and will keep GitLab's code base current. Actual gains will be analyzed once this change is deployed to production.

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Since May, the Growth has been flat. 
    - The intial Growth phase might be complete. 
    - Futrue growth will be depend on features enableing more and deeper use cases. Outlined in [Code Search for Big Code](https://gitlab.com/groups/gitlab-org/-/epics/6220).
  implementation:
    status: Complete
    reasons:
  lessons:
   learned:
    - The [Recently Searched/Used Projects and Groups will be added to the dropdowns on the search results page.](https://gitlab.com/gitlab-org/gitlab/-/issues/239384/?version=216030)
    - We are focused on decreasing [page load times.](https://gitlab.com/gitlab-org/gitlab/-/issues/327106) This is a key elemnt to contiuned growth. 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Global Search - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search)
    - Error Budget was Exceeded for Global Search since May 2021. This is improved and are on track to be within budget or close to budget by Sept. [MRs and issues listed](https://gitlab.com/gitlab-org/gitlab/-/issues/331866)
  sisense_data:
    chart: 12197845
    dashboard: 892802
    embed: v2
  sisense_data_secondary:
    chart: 12197849
    dashboard: 892802
    embed: v2
    
- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - The query Apdex has decreased in all charts after the release of 13.10, including [the weekly one](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=9885649&udv=0), which has a pretty constant number of instances reporting stats. No similar trend on the GitLab.com charts, nor any other clear signal indicating what may be affecting the Apdex. This is still present in 14.1 but the team has not focused on an investigation due to capacity constraints. Current focus is mitigating the Primary Key Overflow risk and addressing stability concerns on GitLab.com.
    - We expect the updates performed in the scope of [Research Database queries for performance and frequency](https://gitlab.com/groups/gitlab-org/-/epics/5652) to further improve the ratio of queries which complete within 250ms and lower the variance of the Apdex even further.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/64e6acku) and [50ms - Tolerable 100ms](https://tinyurl.com/4mjw5azv) for master) but reflects with sharp drops the production incidents that related to the database.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Database - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-database/stage-groups-group-dashboard-enablement-database)
  sisense_data:
    chart: 12570537
    dashboard: 892802
    embed: v2
  sisense_data_secondary:
    chart: 12570541
    dashboard: 892802
    embed: v2

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 145000
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Paid Monthly Active Users on GitLab.com increased by 1.9% in July to 135K exceeding our Q2 target of 134K. A new target has been set for Q3.
    - The slowed growth rate in July is typical for this time of year and we would expect this to rebound starting in September.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Need to account for seasonal fluctuations into target projections. 
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2

